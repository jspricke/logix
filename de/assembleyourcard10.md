---
title: Das card10 zusammenbauen
---

Dein card10 kommt mit all seinen Komponenten und einem [flyer/booklet/Handbuch](/media/assembly_flyer.pdf). Du kannst dir ausserdem unser Zusammenbau-[video-tutorial](/vid/) anschauen.

Die Hauptkomponenten deines Card10 sind zwei elektronische Boards, das _fundamental_-Board und das _harmonic_-Board.
Das _fundamental_-Board sitzt direkt auf dem Armband, das _harmonic_-Board sitzt darüber.
Wenn du mehr über die Boards lernen willst, kannst du eine detailliertere Beschreibung auf der [Hardware-Übersicht](/en/hardware-overview/)-Seite finden.
Wenn du die Tüte auspackst und das Board zusammensetzt, sorge dafür dass die Schutzfolie auf dem Display bleibt, bis du dir die Sektion zu 
[polarisierenden Displays](/de/faq/#why-should-i-consider-keeping-the-protective-foil-on-the-display) in der FAQ angeschaut hast.

<img class="center" alt="harmonic board with glue dot" src="/media/assemble/IMG_20190819_143216.jpg"  width="420" height="auto" align="center">

- Benutze den Klebepunkt, um die Batterie auf das Harmonic-Board zu kleben

<img class="center" alt="battery attached to harmonic board" src="/media/assemble/IMG_20190819_143128.jpg"  width="420" height="auto" align="center"> 

- Stecke die Batterie ein

<img class="center" alt="attaching the spacers" src="/media/assemble/IMG_20190819_142944.jpg"  width="420" height="auto" align="center"> 

- Schraube die Abstandshalter mit den vier Schrauben an.
- Stecke den nylon-Abstandshalter neben LED3. Der Nylon-Abstandshalter ist nicht symmetrisch, die flache Seite sollte in Richtung der Fundamental-Board-Schraube zeigen. Sei vorsichtig und zieh die Schraube auf dem Nylon-Abstandshalter nicht zu fest an, sonst riskierst du, dass der Faden im Abstandshalter beschädigt wird und die Schraube sich lockert.
 
<img class="center" alt="both boards placed on top of each other" src="/media/assemble/IMG_20190819_143114.jpg"  width="420" height="auto" align="center"> 

- Stecke die beiden Boards vorsichtig zusammen.

<img class="center" alt="wristband with card10" src="/media/assemble/IMG_20190819_142436.jpg"  width="420" height="auto" align="center"> 

- lege das Armband hin, mit der flauschigen Seite nach oben, und lege die Metall-stäbe auf das Badge und schraube die übrigen Schrauben an.
 

<img class="center" alt="wristband with card10" src="/media/assemble/IMG_20190819_142653.jpg"  width="420" height="auto" align="center"> 



## Viel Spass mit deinem card10!
Jetzt ist es Zeit, dein card10 [anzuschalten](/de/gettingstarted/#card10-navigation).

<img class="center" alt="happy hacking" src="/media/assemble/IMG_m6w9xw.jpg"  width="420" height="auto" align="center"> 
