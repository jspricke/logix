---
title: Display own Name
---

The card10 can be used to show the own name in big(-ish) letters on the homescreen/lockscreen.

Since text entry on the card10 is cumbersome, it can be set/changed from the apps, and maybe also via USB.

For this reason, the name is exposed as a private BTLE property. Once authorized/paired, an app can read the name and change it. The property shall be "live", meaning that if the name is changed via some other means, it should update via BTLE notification.

As a data format, I propose protobuf with a simple UTF8 string. It will allow us to add e.g. font styling or fancy animations later without a breaking change.

This usecase is designed to be a small usecase such that all aspects of BTLE communication are covered. It allows us to get communication right as early as possible. Other, more complicated usecases and data formats can be added later if we're confident.
