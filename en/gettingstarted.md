# getting started

You just received your card10 - what now?

## card10 assembly

[Here](/en/assembleyourcard10)'s a step by step guide with pictures for assembling your card10

## card10 navigation
There are four buttons on the card10 _harmonic_ board: On the top left, just above the USB-C connector is the POWER button. Below is the LEFT button. The buttons on the right side are closer together. On this side, the top button is the SELECT button, the one below it is the RIGHT button.
 
<img class="center" alt="Drawing of card10 with button names" src="/media/card10buttons.svg"  width="220" height="auto" align="center">


### switching your card10 on and off

To switch on card10, briefly press the POWER button. To switch it off again, press and hold down the power button for several seconds (usually this is around three seconds, however for some apps it can take over ten seconds).

### starting apps

When your card10 is switched on, a short press of the POWER button
brings you to the app menu. You can scroll down with the RIGHT button, 
and up with the LEFT button. The SELECT button starts the selected app. 
The POWER button starts the app called 'main.py', which is also the app 
that will automatically start when you turn on your card10.

### USB storage

Your card10 can function similar to a USB stick, if you start it in _USB storage mode_. You can use this mode to upload _configuration files_ and _micropython scripts_. To enter _USB storage mode_, first switch off your card10 with a long press of the POWER button. Next, hold down the RIGHT button and the POWER button, until your display shows the text "USB activated. Ready.".

Now when you connect the card10 to your laptop via USB, it will show up as a storage device. Don't forget to unmount the card10 storage device.
The USB storage mode is exited by briefly pressing the POWER button.

### setting your nickname
To set your nickname, create a file called `nickname.txt` on your laptop. 
Start your card10 in USB storage mode, then copy `nickname.txt` onto the card10. 
Now look for an app called 'nickname.py' (TODO: this app might need downloading first...), 
which will display the nickname.

### activate BLE
Initially, BLE is disabled for privacy reasons. If you want to use it, you have to activate it first.

  * Via the menu: Select the ble.py from the menu and press the SELECT button to enable BLE. The board will restart with activated BLE. Deactivation can be done via the same app.
  * Manually: Start the card10 in _USB storage mode_ and add a file `ble.txt` with (exactly) the content `active=true` then reset the board.
  
### making LEDs blink



